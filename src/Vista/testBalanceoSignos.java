/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.Pila.Pila;
import java.util.Scanner;
/**
 *
 * @author DOCENTE
 */
public class testBalanceoSignos {
    public static void main(String[] args) {
        System.out.println("Validando [{()}] ");
        /**
         * Ejemplo: L={aa} => --> V
         * L=[{aa}] => --> F
         * L=(a)[a]{b}--> V
         */
        Scanner t = new Scanner(System.in);
        String s = t.nextLine();
        
        try{
            
        if(validarAgrupamiento(s)){
            System.out.println("Verdadero");
        }    
        
        else{
            System.out.println("Falso");
            }
        }
        catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        
        
        
    }
    
    private static boolean validarAgrupamiento(String dato){
        Pila<Character> pila = new Pila();
        
        if(dato.isEmpty()){
            throw new RuntimeException("La cadena está vacía");
        }
        
        for(int i = 0; i < dato.length(); i++){
            
            char elemento = dato.charAt(i);
            
            if(signoAgrupamiento(elemento)){
                
                if(signoAbierto(elemento)){
                    if(pila.isVacia() || jerarquiaSignos(pila.peek(), elemento))
                    pila.push(elemento);
                }
                
                else{
                    
                    if(pila.isVacia()){
                        return false;
                    }
                    
                    char elementoOpen = pila.pop();
                    
                    if(!iguales(elementoOpen, elemento)){
                        return false;
                    }
                }
            }
        }
        
        
        return pila.isVacia();
    }
    
    private static boolean signoAgrupamiento(char c){
        return c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}';
    }
    
    private static boolean signoAbierto(char c){
        return c == '(' || c == '[' || c == '{';
    }
    
    private static boolean iguales(char open, char close){
        return (open == '(' && close == ')') ||
               (open == '[' && close == ']') ||
               (open == '{' && close == '}');
    }
    
    private static boolean jerarquiaSignos(char cInicial, char c){
        
        if(cInicial == '('){
            return false;
        }
        
        if(cInicial == '{')
            return c == '(' || c == '{';
        
       return cInicial == '[';
    }
    
}
