/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Vista;

import Util.LCD.Conjunto;

/**
 *
 * @author USER
 */
public class testConjuntoLCD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        Conjunto<Integer> c1 = new Conjunto();
        Conjunto<Integer> c2 = new Conjunto();    
        Conjunto<Integer> c8 = new Conjunto();
        
        
        
        c1.insertar(1);
        c1.insertar(2);
        c1.insertar(3);
        c1.insertar(4);
        
        c2.insertar(4);
        c2.insertar(5);
        c2.insertar(6);
        
        Conjunto<Integer> c3 = c1.getUnion(c2);
        Conjunto<Integer> c4 = c1.getInterseccion(c2);
        Conjunto<Integer> c5 = c1.getDiferencia(c2);
        Conjunto<Integer> c6 = c2.getDiferencia(c1);
        Conjunto<Integer> c7 = c1.getDiferenciaAsimetrica(c2);
        
        System.out.println(c3);
        System.out.println(c4);
        System.out.println(c5);
        System.out.println(c6);
        System.out.println(c7);
        System.out.println(c1.getPotencia());
        
        
        try{
        //En el caso de conjunto vac�o
        Conjunto<Integer> c9 = new Conjunto();
        Conjunto<Integer> c10 = new Conjunto();
        c10.insertar(1);
        c10.insertar(1);
        System.out.println(c9.getUnion(c1));
        System.out.println(c10.getUnion(c1));
        }
        catch(Exception e){
            System.out.println("Error: " + e.getMessage());
   
        }
        
        
        try{
        //En el caso de conjunto con elementos repetidos
        
        Conjunto<Integer> c10 = new Conjunto();
        c10.insertar(1);
        c10.insertar(1);
        
        System.out.println(c10.getUnion(c1));
        }
        catch(Exception e){
            System.out.println("Error: " + e.getMessage());
   
        }
    
    }
}
