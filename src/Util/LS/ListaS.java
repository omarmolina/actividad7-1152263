/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util.LS;

import java.util.Iterator;
import java.util.Objects;

/**
 * https://pythontutor.com/visualize.html#code=%20class%20ListaS%3CT%3E%7B%0A%20%20%0A%20%20%0A%20%20private%20Nodo%3CT%3E%20cabeza%3B%0A%20%20%20%20private%20int%20size%3B%0A%0A%20%20%20%20public%20ListaS%28%29%20%7B%0A%20%20%20%20%20%20this.cabeza%3Dnull%3B%0A%20%20%20%20%20%20this.size%3D0%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20public%20int%20getSize%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20size%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20public%20void%20insertarInicio%28T%20info%29%0A%20%20%20%20%7B%0A%20%20%20%20%20%20%20%20this.cabeza%3Dnew%20Nodo%28info,%20this.cabeza%29%3B%0A%20%20%20%20%20%20%20%20this.size%2B%2B%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20public%20String%20toString%28%29%20%7B%0A%20%20%20%20%20%20%20%20if%20%28this.isVacia%28%29%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20return%20%22Lista%20Vac%C3%ADa%22%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20String%20msg%20%3D%20%22%22%3B%0A%20%20%20%20%20%20%20%20for%20%28Nodo%3CT%3E%20i%20%3D%20this.cabeza%3B%20i%20!%3D%20null%3B%20i%20%3D%20i.getSig%28%29%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20msg%20%2B%3D%20i.toString%28%29%20%2B%20%22-%3E%22%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20return%20msg%20%2B%20%22null%22%3B%0A%0A%20%20%20%20%7D%0A%0A%20%20%20%20public%20boolean%20isVacia%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20this.cabeza%20%3D%3D%20null%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20public%20void%20insertarFin%28T%20info%29%20%7B%0A%20%20%20%20%20%20%20%20if%20%28this.isVacia%28%29%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20this.insertarInicio%28info%29%3B%0A%20%20%20%20%20%20%20%20%7D%20else%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20Nodo%3CT%3E%20x%3DgetPos%28this.size%20-%201%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20x.setSig%28new%20Nodo%28info,%20null%29%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20this.size%2B%2B%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%0A%20%20%20%20private%20Nodo%3CT%3E%20getPos%28int%20posicion%29%20%7B%0A%20%20%20%20%20%20%20%20this.validar%28posicion%29%3B%0A%20%20%20%20%20%20%20%20Nodo%3CT%3E%20x%20%3D%20this.cabeza%3B%0A%20%20%20%20%20%20%20%20//%20a%3D3,%20b%3Da%3B%20b%3D3%0A%20%20%20%20%20%20%20%20while%20%28posicion--%20%3E%200%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%20%20%20%20%20%20%20%20x%20%3D%20x.getSig%28%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20//pos%3Dpos-1%20%3A%20--%3E%20first%20class%20OP%20%3A%28%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20return%20x%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20private%20void%20validar%28int%20i%29%20%7B%0A%20%20%20%20%20%20%20%20if%20%28this.isVacia%28%29%20%7C%7C%20i%20%3C%200%20%7C%7C%20i%20%3E%3D%20this.size%29%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20throw%20new%20RuntimeException%28%22No%20es%20v%C3%A1lida%20la%20posici%C3%B3n%22%29%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%20%20%0A%20%20%7D%0A%0A%0A%20class%20Nodo%3CT%3E%7B%0A%20%20%0A%20%20private%20T%20info%3B%0A%20%20private%20Nodo%3CT%3E%20sig%3B%0A%20%20%0A%20%20%0A%20%20Nodo%28%29%20%7B%0A%20%20%20%20%7D%0A%0A%20%20%20%20Nodo%28T%20info,%20Nodo%3CT%3E%20sig%29%20%7B%0A%20%20%20%20%20%20%20%20this.info%20%3D%20info%3B%0A%20%20%20%20%20%20%20%20this.sig%20%3D%20sig%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20T%20getInfo%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20info%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20void%20setInfo%28T%20info%29%20%7B%0A%20%20%20%20%20%20%20%20this.info%20%3D%20info%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20Nodo%3CT%3E%20getSig%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20sig%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20void%20setSig%28Nodo%3CT%3E%20sig%29%20%7B%0A%20%20%20%20%20%20%20%20this.sig%20%3D%20sig%3B%0A%20%20%20%20%7D%0A%20%20%20%20%0A%20%20%20%20public%20String%20toString%28%29%20%7B%0A%20%20%20%20%20%20%20%20return%20info.toString%28%29%3B%0A%20%20%20%20%7D%0A%0A%20%20%7D%0A%0A%0A%0A%0A//Paquete%20vista%0Apublic%20class%20TestListaS%20%7B%0A%20%20%20%20public%20static%20void%20main%28String%5B%5D%20args%29%20%7B%0A%20%20%20%20%20%20%0A%20%20%20%20%20%20//Clase%20parametrizada%20%3A%20Integer%0A%20%20%20%20%20%20ListaS%3CInteger%3E%20lista%3Dnew%20ListaS%28%29%3B%0A%20%20%20%20%20%20lista.insertarInicio%284%29%3B%0A%20%20%20%20%20%20lista.insertarInicio%285%29%3B%0A%20%20%20%20%20%20lista.insertarInicio%286%29%3B%0A%20%20%20%20%20%20lista.insertarFin%287%29%3B%0A%20%20%20%20%20%20System.out.println%28lista.toString%28%29%29%3B%0A%0A%20%20%20%20%7D%0A%7D&cumulative=false&curInstr=101&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=java&rawInputLstJSON=%5B%5D&textReferences=false
 *
 * @author Estudiantes
 * @param <T>
 *
 */
public class ListaS<T> implements Iterable<T>, Comparable<T>{

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null;
        this.size = 0;
    }

    public int getSize() {
        return size;
    }

    public void insertarInicio(T info) {

        this.cabeza = new Nodo(info, this.cabeza);
        this.size++;
    }

    public void insertarFin(T info) {
        if (this.isVacia()) {
            this.insertarInicio(info);
        } else {
            getPos(this.size - 1).setSig(new Nodo(info, null));
            this.size++;
        }
    }

    private Nodo<T> getPos(int posicion) {
        this.validar(posicion);
        Nodo<T> x = this.cabeza;
        // a=3, b=a; b=3
        while (posicion-- > 0) {

            x = x.getSig();
            //pos=pos-1 : --> first class OP :(
        }
        return x;
    }

    private void validar(int i) {
        if (this.isVacia() || i < 0 || i >= this.size) {
            throw new RuntimeException("No es válida la posición");
        }
    }

    @Override
    public String toString() {
        if (this.isVacia()) {
            return "Lista Vacia";
        }
        String msg = "";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.toString();
            
            if(i.getSig() != null){
                msg += ", ";
            }
        }
        return msg;

    }

    public boolean isVacia() {
        return this.cabeza == null;
    }

    public T get(int i) {
        try {
            this.validar(i);
            Nodo<T> pos = this.getPos(i);
            return pos.getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public void set(int i, T info) {
        try {
            this.validar(i);
            Nodo<T> pos = this.getPos(i);
            pos.setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }

    public T remove(int i) {
        this.validar(i);
        Nodo<T> borrar = this.cabeza;
        if (i == 0) {
            this.cabeza = this.cabeza.getSig();

        } else {
            Nodo<T> anterior = this.getPos(i - 1);
            borrar = anterior.getSig();
            anterior.setSig(borrar.getSig());
        }

        this.size--;
        borrar.setSig(null);
        return borrar.getInfo();
    }

    public boolean addAll(ListaS<T> l2) {
        if (this.isVacia() && l2.isVacia()) {
            return false;
        }

        if (l2.isVacia()) {
            return false;
        }

        if (this.isVacia()) {
            this.cabeza = l2.cabeza;

        } else {
            Nodo<T> ultimo = this.getPos(size - 1);
            ultimo.setSig(l2.cabeza);

        }

        this.size += l2.size;
        l2.clear();
        return true;
    }

    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    public boolean isPalin() {
        if (this.isVacia()) {
            throw new RuntimeException("No se puede realizar el proceso");
        }

        ListaS<T> l2 = crearInvertida();
        return (this.equals(l2));

    }

    public ListaS<T> crearInvertida() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        ListaS<T> l2 = new ListaS();
        Nodo<T> ultimo_l2 = null;
        for (Nodo<T> actual = this.cabeza; actual != null; actual = actual.getSig()) {
            T info = actual.getInfo();
            Nodo<T> nuevo_l2 = new Nodo(info, ultimo_l2);
            ultimo_l2 = nuevo_l2;
        }
        l2.cabeza = ultimo_l2;
        l2.size = this.size;
        return l2;

    }

    public void invertir() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        if (this.size == 1) {
            return;
        }

        Nodo<T> ult = null;
        Nodo<T> actual = this.cabeza;
        Nodo<T> sig;
        while (actual != null) {
            sig = actual.getSig();
            actual.setSig(ult);
            ult = actual;
            actual = sig;

        }

        this.cabeza = ult;

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.cabeza);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaS<T> other = (ListaS<T>) obj;
        if (this.isVacia() || other.isVacia()) {
            return false;
        }
        if (this.size != other.size) {
            return false;
        }
        // T debe tener implementado equals
        for (Nodo<T> c = this.cabeza, c2 = other.cabeza; c != null; c = c.getSig(), c2 = c2.getSig()) {
            if (!c.getInfo().equals(c2.getInfo())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Iterator<T> iterator() {
            return new IteratorLS(this.cabeza);
    }
    
   
    public boolean contains(Object o) {
        /*for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            if((i.getInfo() == null && o == null)){
                return true;
            }
            
            if(i.getInfo().equals(o)){
                return true;
            }
        }
        return false;*/

        return this.indexOf(o) != -1;
    }

    public int indexOf(Object o) {

        int indexActual = 0;

        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            if (Objects.equals(i.getInfo(), o)) {
                return indexActual;
            }

            indexActual++;
        }

        return -1;
    }
    
    public ListaS<T> clonar(){
        
        ListaS<T> l1 = new ListaS();
        for(Nodo<T> i = this.cabeza; i != null; i = i.getSig()){
            
            l1.insertarFin(i.getInfo());
        }
        
        return l1;
    }
    
    public boolean contieneElemRepetidos(){
        /*if (this.isVacia()) 
            throw new RuntimeException("No es válido operación");*/
        
            
        for (Nodo<T> i = this.cabeza; i.getSig() != null; i = i.getSig()) {
            Nodo<T> siguiente = i.getSig();
            
            for (Nodo<T> j = siguiente; j != null; j = j.getSig()) {
                if (i.getInfo().equals(j.getInfo())) {
                    return true;
                }

            }
        }
            
        return false;
    }

    @Override
    public int compareTo(T o) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
   
    
    
    
    

}
