/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.LS;

import java.util.Iterator;

/**
 *
 * @author docente
 */
public class IteratorLS<T> implements Iterator<T> {

    private Nodo<T> inicio;

    public IteratorLS(Nodo<T> cabeza) {
        /*if (cabeza == null) {
            throw new RuntimeException("No se puede crear");
        }*/
        
        this.inicio = cabeza;
    }

    @Override
    public boolean hasNext() {
        return this.inicio != null;
    }

    @Override
    public T next() {
        if (this.hasNext()) {
            T info = this.inicio.getInfo();
            this.inicio = this.inicio.getSig();
            return info;
        }
        return null;
    }

}
