/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.LCD;

import java.util.Iterator;

/**
 *
 * @author DOCENTE
 */
public class ListaCD<T> implements Iterable<T> {

    private NodoD<T> cabeza;
    private int size = 0;

    public int getSize() {
        return size;
    }

    public ListaCD() {

        this.cabeza = new NodoD(null, null, null);
        this.cabeza.setAnt(cabeza);
        this.cabeza.setSig(cabeza);
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza.getSig(), this.cabeza);
        this.cabeza.setSig(nuevo);
        nuevo.getSig().setAnt(nuevo);
        this.size++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.size++;
    }

    @Override
    public String toString() {
        String msg = "";
        for (NodoD<T> x = this.cabeza.getSig(); x != this.cabeza; x = x.getSig()) {
            msg += x.getInfo() + "<->";
        }
        return "cab<->" + msg + "cab";
    }

    public boolean isVacia() {
        //this.size==0
        //this.cabeza==this.cabeza.getAnt()
        return this.cabeza == this.cabeza.getSig();
    }

    public T remove(int i) {
        
        validar(i);
        
        NodoD<T> borrar = this.getPos(i);
        borrar.getAnt().setSig(borrar.getSig());
        borrar.getSig().setAnt(borrar.getAnt());
        
        this.size--;
        
        borrar.setAnt(null);
        borrar.setSig(null);
        return borrar.getInfo();
    }

    private void validar(int i) {
        if (this.isVacia() || i < 0 || i >= this.size) {
            throw new RuntimeException("No es válida la posición");
        }
    }

    private NodoD<T> getPos(int posicion) {
        this.validar(posicion);
        NodoD<T> x = this.cabeza.getSig();
        while (posicion-- > 0) {

            x = x.getSig();
        }
        return x;
    }

    public T get(int i) {
        try {
            this.validar(i);
            NodoD<T> pos = this.getPos(i);
            return pos.getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public void set(int i, T info) {
        try {
            this.validar(i);
            NodoD<T> pos = this.getPos(i);
            pos.setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }
    
    /**
     * Permite limpiar una lista, haciendo que el siguiente y el anterior de la cabeza sea la misma cabeza, así como el tamaño
     * pasa a 0
     */
    public void clear() {
        this.cabeza.setSig(this.cabeza);
        this.cabeza.setAnt(this.cabeza);
        this.size = 0;
    }
    
    
    
    
    /**
     * Añade toda una lista l2 (del parámetro) a otra lista (agrega a lo último) -- l2 desaparee
     * @param l2 
     * @return boolean
     */
    public boolean addAll(ListaCD<T> l2){
        
        if(this.isVacia() && l2.isVacia()){
            return false;
        }
        
        if(l2.isVacia()){
            return false;
        }
        
        this.cabeza.getAnt().setSig(l2.cabeza.getSig());
        l2.cabeza.getSig().setAnt(this.cabeza.getAnt());

        l2.cabeza.getAnt().setSig(this.cabeza);
        this.cabeza.setAnt(l2.cabeza.getAnt());
            
        this.size += l2.size;
        
        l2.clear();
  
        return true;
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorLCD(this.cabeza);
    }
    
    
    public ListaCD<T> clonar(){
        
        ListaCD<T> result = new ListaCD();
        
        for(NodoD<T> i = this.cabeza.getSig(); i != this.cabeza; i = i.getSig()){
            result.insertarFin(i.getInfo());
        }
        
        return result;
    }

    /**
     * Permite la intersección entre dos listas
     * @param l1
     * @return ListaCD<T>
     */
    public ListaCD<T> getInterseccion(ListaCD<T> l1) {
        if(this.isVacia() || l1.isVacia()){
            throw  new RuntimeException("Al menos una de las listas esta vacia");
        }
        ListaCD<T> result = new ListaCD();
        ListaCD<T> mayor = this;
        ListaCD<T> menor = l1;
        if(this.size < l1.size){
            mayor = l1;
            menor = this;
        }
        Iterator<T> it = menor.iterator();
        while(it.hasNext()){
            T info = it.next();
            if(mayor.contains(info)){
                result.insertarFin(info);
            }
        }
        return result;
    }

    public boolean contains(T info) {
        
        Iterator<T> it = this.iterator();
        while (it.hasNext()) {
            if (it.next().equals(info)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Permite conocer si una lista tiene elementos repetidos
     * @return boolean
     */
    public boolean contieneRepetidos(){
        if(this.isVacia()){
            throw new RuntimeException("No es valida la operación");
        }
        
        for(NodoD<T> i = this.cabeza.getSig(); i != this.cabeza; i = i.getSig()){
            
            NodoD<T> sig = i.getSig();
            
            for(NodoD<T> j = sig; j != this.cabeza; j = j.getSig()){
                if(i.getInfo().equals(j.getInfo())){
                    return true;
                }
            }
        }
        return false;
    }
    
    
    public void concat(ListaCD<T> l1, int i){
        //l1 queda vacío
        //no crear nodos
        //inserta despues de i
        
        NodoD<T> actual = this.cabeza.getSig();
        int index = 0;
        
        while(actual != this.cabeza && index < i){
            actual = actual.getSig();
            index++;
        }
        
        if(actual == this.cabeza){
            throw new RuntimeException("El índice está fuera de los límites");
        }
        
        NodoD<T> primero = l1.cabeza.getSig();
        NodoD<T> ultimo = l1.cabeza.getAnt();
        
        actual.getSig().setAnt(ultimo);
        ultimo.setSig(actual.getSig());
        
        actual.setSig(primero);
        primero.setAnt(actual);
        
        this.size += l1.getSize();
        
        l1.clear();
    }
    
   
    
    
 
   
}
