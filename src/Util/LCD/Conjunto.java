/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.LCD;



/**
 *  Wrapper (Envoltorio)
 * @author docente
 */


public class Conjunto<T> implements IConjunto<T>{
   
    private ListaCD<T> elementos;

    public Conjunto() {
        this.elementos = new ListaCD();
    }

    
    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        
        if(this.esVacio()){
            throw new RuntimeException("El conjunto es vacio");
        }
        
        if(verificarRepetidos(this) || verificarRepetidos(c1)){
            throw new RuntimeException("Hay elementos repetidos en los conjuntos");
        }
        
        Conjunto<T> resultado = new Conjunto();
       
        for(T elemento: this.elementos){
            resultado.insertar(elemento);
        }
        
        for(T elemento: c1.elementos){
            if(!this.contiene(elemento)){
                resultado.insertar(elemento);
            }
        }
 
        return resultado;
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        
        if(this.esVacio()){
            throw new RuntimeException("El conjunto es vacio");
        }
        
        if(verificarRepetidos(this) || verificarRepetidos(c1)){
            throw new RuntimeException("Hay elementos repetidos en los conjuntos");
        }
        
        Conjunto<T> resultado = new Conjunto();
        Conjunto<T> mayor = this;
        Conjunto<T> menor = c1;
        
        if(this.elementos.getSize() < c1.elementos.getSize()){
            mayor = c1;
            menor = this;
        }
        
        for(T elemento: menor.elementos){
            if(mayor.contiene(elemento)){
                resultado.insertar(elemento);
            }
        }
        return resultado;
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        
        if(this.esVacio()){
            throw new RuntimeException("El conjunto es vacio");
        }
        
        if(verificarRepetidos(this) || verificarRepetidos(c1)){
            throw new RuntimeException("Hay elementos repetidos en los conjuntos");
        }
        
        Conjunto<T> resultado = new Conjunto();
        
        for(T elemento: this.elementos){
            if(!c1.contiene(elemento)){
                resultado.insertar(elemento);
            }
        }
        
        return resultado;
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        
        if(this.esVacio()){
            throw new RuntimeException("El conjunto es vacio");
        }
        
        if(verificarRepetidos(this) || verificarRepetidos(c1)){
            throw new RuntimeException("Hay elementos repetidos en los conjuntos");
        }
        
        Conjunto<T> resultado = new Conjunto();
        
        Conjunto<T> dif1 = this.getDiferencia(c1);
        Conjunto<T> dif2 = c1.getDiferencia(this);
        
        resultado.insertarConjuntos(dif1);
        resultado.insertarConjuntos(dif2);
        
        return resultado;
    }
    

 
     public Conjunto<Conjunto<T>> getPotencia() {
         
      if(this.esVacio()){
            throw new RuntimeException("El conjunto es vacio");
        }
         
      Conjunto<Conjunto<T>> resultado = new Conjunto();
      resultado.insertar(new Conjunto ());
      int n = this.getSize();
      
      for(int i = 1; i<= n; i++){
          combinaciones(this, 0, i, new Conjunto(), resultado);
      }
        
        return resultado;
    }
    
    private void combinaciones(Conjunto<T> conjunto, int start, int length, Conjunto<T> combinacion, Conjunto<Conjunto<T>> resultado){
        
        if(length == 0){
            resultado.insertar(combinacion.clonar());
            return;
        }
        
        for(int i = start; i<= this.getSize() - length; i++){
            combinacion.insertar(this.get(i));
            combinaciones(this, i + 1, length - 1, combinacion, resultado);
            combinacion.elementos.remove(combinacion.getSize() - 1);
        }
    }
    
    //Ejemplo de getPotencia sin tener en cuenta el orden por longitud
    
    
    public Conjunto<Conjunto<T>> getPotencia2() {
        
        Conjunto<Conjunto<T>> resultado = new Conjunto();
        
        resultado.insertar(new Conjunto());
        
        for(T e : this.elementos){
            int n = resultado.elementos.getSize();
            
            for(int i = 0; i < n; i++){
                Conjunto<T> subConjunto = new Conjunto();
                subConjunto.elementos = resultado.get(i).elementos.clonar();
                
                
                subConjunto.insertar(e);
                resultado.insertar(subConjunto);
                
            }
        }
           
        return resultado;
    }
    
     
    @Override
    public T get(int i) {
        return this.elementos.get(i);
    }

    @Override
    public void set(int i, T info) {
        this.elementos.set(i, info);
    }

    @Override
    public void insertar(T info) {
        this.elementos.insertarFin(info);
    }
    
    public void insertarInicio(T info){
        this.elementos.insertarInicio(info);
    }
    
    @Override
    public void insertarConjuntos(Conjunto<T> c1){
        this.elementos.addAll(c1.elementos);
    }
    
    public int getSize() {
        return this.elementos.getSize();
    }
    
    public Conjunto<T> clonar(){
        Conjunto<T> clonado = new Conjunto();
        clonado.elementos = this.elementos.clonar();
        return clonado;
    }
 
    @Override
    public boolean contiene(T info) {
        return this.elementos.contains(info);
    }
    
    @Override
    public boolean esVacio(){
        return this.elementos.isVacia();
    }
    
    @Override
    public boolean verificarRepetidos(Conjunto<T> c1){
        return this.elementos.contieneRepetidos();
    }
    
    public String toString(){
        return "{" + this.elementos.toString() + "}";
    }
  
    
}
