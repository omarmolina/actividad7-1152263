/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.Pila;

import java.util.Iterator;

/**
 *
 * @author DOCENTE
 */
public class IteratorLCD<T> implements Iterator<T>{

    NodoD<T> nodo;
    NodoD<T> cabeza;

    public IteratorLCD(NodoD<T> cabeza) {
        nodo = cabeza.getSig();
        this.cabeza = cabeza;
    }
    
    
    
    @Override
    public boolean hasNext() {
        return this.nodo != this.cabeza;
    }

    @Override
    public T next() {
        if(this.hasNext()){
            nodo = nodo.getSig();
            return nodo.getAnt().getInfo();
        }
        return null;
    }
    
}
